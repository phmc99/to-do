import { useState } from "react";

export function Form({ addTodo }) {
  const [userInput, setUserInput] = useState("");

  return (
    <div>
      <input
        type="text"
        value={userInput}
        onChange={(event) => setUserInput(event.target.value)}
      />
      <button onClick={() => addTodo(userInput)}>Adicionar</button>
    </div>
  );
}
