export function TodoList({ todos, handleTodo }) {
  return (
    <ul>
      {todos.map((item, index) => (
        <>
          <li key={index}>{item}</li>
          <button onClick={() => handleTodo(item)}>Concluir</button>
        </>
      ))}
    </ul>
  );
}
