import logo from "./logo.svg";
import "./App.css";

import { TodoList } from "./components/ToDoList";
import { Form } from "./components/Form";

import { useState } from "react";

function App() {
  const [todo, setTodo] = useState([]);

  function handleTodo(elt) {
    setTodo(todo.filter((item) => item !== elt));
  }

  function addTodo(newTodo) {
    setTodo([...todo, newTodo]);
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Form addTodo={addTodo} />
        <TodoList todos={todo} handleTodo={handleTodo} />
      </header>
    </div>
  );
}

export default App;
